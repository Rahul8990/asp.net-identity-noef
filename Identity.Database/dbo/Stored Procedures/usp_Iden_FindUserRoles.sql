﻿CREATE PROC [dbo].[usp_iden_FindUserRoles] @USERID UNIQUEIDENTIFIER
AS
BEGIN
	SELECT IR.NAME
	FROM IdentityRole IR
	INNER JOIN IdentityUserRole IUR ON IR.RoleId = IUR.RoleId
		AND IUR.UserId = @USERID
END