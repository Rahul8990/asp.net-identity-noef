﻿CREATE PROC [dbo].[usp_iden_AddUserToRole] @USERID UNIQUEIDENTIFIER
	,@ROLENAME NVARCHAR(256)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		-- get role id
		DECLARE @ROLEID UNIQUEIDENTIFIER = (
				SELECT RoleId
				FROM IdentityRole
				WHERE NAME = @ROLENAME
				)

		-- insert, if not exist
		INSERT INTO IdentityUserRole (
			UserId
			,RoleId
			)
		SELECT IU.UserId
			,@ROLEID
		FROM IdentityUser IU
		LEFT JOIN IdentityUserRole IUR ON IU.UserId = IUR.UserId
			AND IUR.RoleId = @ROLEID
		WHERE IU.UserId = @USERID
			AND IUR.UserId IS NULL

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		IF @@ERROR <> 0
			AND @@TRANCOUNT > 0
			ROLLBACK TRANSACTION

		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
			,@ErrorNumber INT = ERROR_NUMBER()
			,@ErrorSeverity INT = ERROR_SEVERITY()
			,@ErrorState INT = ERROR_STATE()
			,@ErrorLine INT = ERROR_LINE()
			,@ErrorProcedure NVARCHAR(200) = ISNULL(ERROR_PROCEDURE(), '-');

		SELECT @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 'Message: ' + @ErrorMessage;

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,1
				,@ErrorNumber
				,@ErrorSeverity
				,@ErrorState
				,@ErrorProcedure
				,@ErrorLine
				)
			--THROW --if on SQL2012 or above
	END CATCH
END