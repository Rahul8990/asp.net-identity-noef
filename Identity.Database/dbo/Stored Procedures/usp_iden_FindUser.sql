﻿CREATE PROC usp_iden_FindUser @USERID UNIQUEIDENTIFIER
AS
BEGIN
	SELECT IU.* -- identity user
	FROM IdentityUser IU
	WHERE IU.UserId = @USERID;

	SELECT IP.* -- identiy profile
	FROM IdentityProfile IP
	WHERE IP.UserId = @USERID;

	SELECT IR.* -- identiy profile
	FROM IdentityRole IR
	INNER JOIN IdentityUserRole IUR ON IR.RoleId = IUR.RoleId
	WHERE IUR.UserId = @USERID;
END