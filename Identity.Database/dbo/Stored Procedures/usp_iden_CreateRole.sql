﻿CREATE PROC [dbo].[usp_iden_CreateRole] @ROLEID UNIQUEIDENTIFIER
	,@NAME NVARCHAR(256)
AS
BEGIN
	INSERT INTO [dbo].[IdentityRole] (
		[RoleId]
		,[Name]
		)
	VALUES (
		@ROLEID
		,@NAME
		)
END
